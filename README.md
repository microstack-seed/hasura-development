# hasura-migrations

To start your development environment:

## Prerequisites

- Latest Docker installed
- Docker Compose installed


#

## Launching the environment

1. Make sure our start and stop scripts are executable (windows machines will ned a bash interpter such a [cygwin:https://www.cygwin.com/])

    ```chmod +x *.sh```

2. Make sure you have Docker running. The following should show a process table wtihout any errors

    ```docker ps```

3. Now we simply type the following to spin up our Dev Stack

    ```./start.sh```

4. To work within Hasura visit the console @ http://localhost:8080

(OPTIONAL) This project also include a node project that will install the Hasura cli. Simply run: ```npm install``` to add it. We plan to use automated migration but this could be useful for other scenarios.

#

## Development Best Practices

 - It's recommended to have your .Net Hasura-Proxy project already loaded and running. This will ensure Hasura can fetch and validate any hooks, stitched schemas. Hasura may report Metadata issues if not. If this happens, simply start your Hasura-Proxy and refresh/reload the Metadata in the console.

- Since all developers share the same DB instance it is important that they communicate changes to one another and ask others to refresh their consoles as needed to stay in sync on the evolution of the service.

#

## Additional Details

- We use a special stack for development that helps us mimic the higher-level environment stack while being able to use our local IDE's to code, build and debug. This stack uses explicit naming conventions to ensure our Hasura Graph Metadata remains consistent through environment promotion.

- All developers should be pointed to the same backend DB so that both developers can stay perfectly insync with one another as we add new tables, action, etc...

- If you need to run your .Net code on a differrent a different port than the default 5000 you can change this in the ```.env``` file located at the root of this project. This is also where developers can update the DB settings should they change.